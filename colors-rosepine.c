static const char normal_background[] = "#191724";
static const char mode_background[]   = "#1f1d2e";
static const char dim_background[]    = "#26233a";
static const char active_background[] = "#403d52";

static const char normal_foreground[] = "#908caa";
static const char active_foreground[] = "#e0def4";
static const char dim_foreground[]    = "#6e6a86";

static const char normal_border[]  = "#403d52";
static const char active_border[]  = "#eb6f92";

static const char normal_background[] = "#2e3440";
static const char mode_background[]   = "#3b4252";
static const char dim_background[]    = "#434c5e";
static const char active_background[] = "#4c566a";

static const char normal_foreground[] = "#d8dee9";
static const char active_foreground[] = "#eceff4";
static const char dim_foreground[]    = "#818997";

static const char normal_border[]  = "#4c566a";
static const char active_border[]  = "#eceff4";

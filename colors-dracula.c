static const char normal_background[] = "#282A36";
static const char mode_background[]   = "#323545";
static const char dim_background[]    = "#3C4055";
static const char active_background[] = "#51546D";

static const char normal_foreground[] = "#F6F6F0";
static const char active_foreground[] = "#ffffff";
static const char dim_foreground[]    = "#A2A9CB";

static const char normal_border[]  = "#44475A";
static const char active_border[]  = "#50FA7B";

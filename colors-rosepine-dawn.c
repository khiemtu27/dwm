static const char normal_background[] = "#faf4ed";
static const char mode_background[]   = "#f4ede8";
static const char dim_background[]    = "#f2e9e1";
static const char active_background[] = "#b4637a";

static const char normal_foreground[] = "#797593";
static const char active_foreground[] = "#fffaf3";
static const char dim_foreground[]    = "#9893a5";

static const char normal_border[]  = "#f2e9e1";
static const char active_border[]  = "#d7827e";

static const char normal_background[] = "#1d2021";
static const char mode_background[]   = "#282828";
static const char dim_background[]    = "#3a3634";
static const char active_background[] = "#504945";

static const char normal_foreground[] = "#d5c4a1";
static const char active_foreground[] = "#fbf1c7";
static const char dim_foreground[]    = "#7c6f64";

static const char normal_border[]  = "#3c3836";
static const char active_border[]  = "#ececec";

static const char normal_background[] = "#eff1f5";
static const char mode_background[]   = "#e6e9ef";
static const char dim_background[]    = "#dce0e8";
static const char active_background[] = "#ccd0da";

static const char normal_foreground[] = "#4c4f69";
static const char active_foreground[] = "#2c2f49";
static const char dim_foreground[]    = "#8c8fa1";

static const char normal_border[]  = "#acb0be";
static const char active_border[]  = "#40a02b";

static const char normal_background[] = "#ffffff";
static const char mode_background[]   = "#f6f6f6";
static const char dim_background[]    = "#ededed";
static const char active_background[] = "#e4e4e4";

static const char normal_foreground[] = "#707070";
static const char active_foreground[] = "#252525";
static const char dim_foreground[]    = "#a9a9a9";

static const char normal_border[]  = "#9f9f9d";
static const char active_border[]  = "#62A0C4";

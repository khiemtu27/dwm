static const char normal_background[] = "#1a1b26";
static const char mode_background[]   = "#24283b";
static const char dim_background[]    = "#414868";
static const char active_background[] = "#565f89";

static const char normal_foreground[] = "#a9b1d6";
static const char active_foreground[] = "#c0caf5";
static const char dim_foreground[]    = "#9aa5ce";

static const char normal_border[]  = "#414868";
static const char active_border[]  = "#b4f9f8";

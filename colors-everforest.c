static const char normal_background[] = "#1e2326";
static const char mode_background[]   = "#272e33";
static const char dim_background[]    = "#2e383c";
static const char active_background[] = "#374145";

static const char normal_foreground[] = "#d3c6aa";
static const char active_foreground[] = "#ffffff";
static const char dim_foreground[]    = "#7a8478";

static const char normal_border[]  = "#495156";
static const char active_border[]  = "#a7c080";

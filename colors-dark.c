/* static const char normal_background[] = "#1f1f1f"; */
static const char normal_background[] = "#000000";
static const char mode_background[]   = "#232323";
static const char dim_background[]    = "#2f2f2f";
static const char active_background[] = "#3d3d3d";

static const char normal_foreground[] = "#dedede";
static const char active_foreground[] = "#ffffff";
static const char dim_foreground[]    = "#bbbbbb";

static const char normal_border[]  = "#3f3f3f";
static const char active_border[]  = "#ececec";

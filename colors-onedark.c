static const char normal_background[] = "#282c34";
static const char mode_background[]   = "#333A45";
static const char dim_background[]    = "#444D5E";
static const char active_background[] = "#555F72";

static const char normal_foreground[] = "#abb2bf";
static const char active_foreground[] = "#ffffff";
static const char dim_foreground[]    = "#95A0B1";

static const char normal_border[]  = "#5C6372";
static const char active_border[]  = "#98c379";

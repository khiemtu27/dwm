static const char normal_background[] = "#11111b";
static const char mode_background[]   = "#181825";
static const char dim_background[]    = "#1e1e2e";
static const char active_background[] = "#313244";

static const char normal_foreground[] = "#9399b2";
static const char active_foreground[] = "#cdd6f4";
static const char dim_foreground[]    = "#6c7086";

static const char normal_border[]  = "#313244";
static const char active_border[]  = "#a6e3a1";
